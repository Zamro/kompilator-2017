 /*opcja mówiąca flexowi, że ma przeczytać tylko jeden plik - zastępuje funkcję yywrap, która jest wywoływana w momencie napotkania końca pliku i zwraca 1 jeżeli jest to ostani czytany plik, albo podmienia plik i zwraca 0, jeżeli flex ma kontunuować na nim działanie*/
%option noyywrap
 /*opcja mówiąca flexowi, że ma udostępniać zmienną z numerem aktualnie przetwarzanej linii */
%option yylineno

 /* flex może znajdować się w różnych stanach.
 przed regułami możemy wypisać w nawiasach <> w jakich stanach flex może używać danej reguły
 Domyślnym stanem jest INITIAL, wszystkie reguły, które nie mają dopisanych z przodu dozwolonych stanów początkowych mają właśnie <INITIAL>
stany możemy dodawać na dwa sposoby:
    %s STAN oznacza, że jest on domyślnie dopisywany do wszystkich reguł, czyli stany początkowe dla reguł domyśnie wyglądają tak: <INITIAL, STAN>
    %x STAN oznacza, że nie jest on dopisywany do wszystkich reguł, czyli stany domyślne sie nie zmieniają. 
 definiujemy taki stan: */
%x CCOMMENT
%{
#include <stdio.h>
#include "bisonWrapper.hpp"
YY_DECL;                               /*deklarujemy int yylex(); potrzebne do działania flexa*/

#define yyterminate() return( yy::parser::make_ENDoF() )
%}

%%

 /* zjadamy komentarze */
"("              { BEGIN(CCOMMENT); }
<CCOMMENT>[^\)]  { }
<CCOMMENT>")"    { BEGIN(INITIAL); }

VAR         {DEBUG_PRINT("VAR");        return(yy::parser::make_VAR());}
BEGIN       {DEBUG_PRINT("BEG");        return(yy::parser::make_BEG());}
END         {DEBUG_PRINT("END");        return(yy::parser::make_END());}
IF          {DEBUG_PRINT("IF");         return(yy::parser::make_IF());}
ELSE        {DEBUG_PRINT("ELSE");       return(yy::parser::make_ELSE());}
THEN        {DEBUG_PRINT("THEN");       return(yy::parser::make_THEN());}
ENDIF       {DEBUG_PRINT("ENDIF");      return(yy::parser::make_ENDIF());}
WHILE       {DEBUG_PRINT("WHILE");      return(yy::parser::make_WHILE());}
DO          {DEBUG_PRINT("DO");         return(yy::parser::make_DO());}
ENDWHILE    {DEBUG_PRINT("ENDWHILE");   return(yy::parser::make_ENDWHILE());}
FOR         {DEBUG_PRINT("FOR");        return(yy::parser::make_FOR());}
FROM        {DEBUG_PRINT("FROM");       return(yy::parser::make_FROM());}
ENDFOR      {DEBUG_PRINT("ENDFOR");     return(yy::parser::make_ENDFOR());}
TO          {DEBUG_PRINT("TO");         return(yy::parser::make_TO());}
DOWNTO      {DEBUG_PRINT("DOWNTO");     return(yy::parser::make_DOWNTO());}
READ        {DEBUG_PRINT("READ");       return(yy::parser::make_READ());}
WRITE       {DEBUG_PRINT("WRITE");      return(yy::parser::make_WRITE());}
":="        {DEBUG_PRINT("SET");        return(yy::parser::make_SET());}
"["         {DEBUG_PRINT("LBRACKET");   return(yy::parser::make_LBRACKET());}
"]"         {DEBUG_PRINT("RBRACKET");   return(yy::parser::make_RBRACKET());}
"+"         {DEBUG_PRINT("PLUS");       return(yy::parser::make_PLUS());}
"-"         {DEBUG_PRINT("MINUS");      return(yy::parser::make_MINUS());}
"*"         {DEBUG_PRINT("MULTI");      return(yy::parser::make_MULTI());}
"/"         {DEBUG_PRINT("DIV");        return(yy::parser::make_DIV());}
"%"         {DEBUG_PRINT("MOD");        return(yy::parser::make_MOD());}
"="         {DEBUG_PRINT("EQ");         return(yy::parser::make_EQ());}
"<>"        {DEBUG_PRINT("NEQ");        return(yy::parser::make_NEQ());}
"<"         {DEBUG_PRINT("LESS");       return(yy::parser::make_LESS());}
">"         {DEBUG_PRINT("GREATER");    return(yy::parser::make_GREATER());}
"<="        {DEBUG_PRINT("LEQ");        return(yy::parser::make_LEQ());}
">="        {DEBUG_PRINT("GEQ");        return(yy::parser::make_GEQ());}
[0-9]+      {DEBUG_PRINT("NUM");        return(yy::parser::make_NUM(yytext));}
[a-z]+      {DEBUG_PRINT("PIDENTIFIER");return(yy::parser::make_PIDENTIFIER(yytext));}
";"         {DEBUG_PRINT("COLON");      return(yy::parser::make_COLON());}

[ \t\r\n]+  {}
.           {yyterminate();}
%%
