#include "variable.hpp"

void Variable::set(unsigned long long address, long long size){
	this->address = address;
	this->size = size;
	isInitialised = (size != 0); //sprawdzamy inicjalizację tylko dla skalarów
}

unsigned long long Variable::getAddress()
{
	return address;
}

