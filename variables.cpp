#include "variables.hpp"

bool Variables::createVariable(std::string id, long long length)
{
    if(variables.count(id))
        return 0;

    variables[id].set(freeAddress, length);
    if(length>0)
        freeAddress += length;
    else
        freeAddress += 1;
    return 1;
}

void Variables::deleteVariable(std::string id)
{
    variables.erase(id);
}

bool Variables::checkId(std::string id)
{
    return variables.count(id);
}

Variable& Variables::get(std::string id)
{
    if( not checkId(id))
        variables[id].isInitialised = true; //hack to remove double error for undeclared variable
    return variables[id];
}

unsigned long long Variables::getFreeAddress() {
    return freeAddress++;
}