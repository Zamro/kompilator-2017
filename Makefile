SHELL := /bin/bash

kompilator: bison.y lex.l
	bison -d bison.y -o bison.tab.cc
	flex lex.l
	g++ -o  kompilator.out lex.yy.c bison.tab.cc variables.cpp variable.cpp --std=c++11 -l cln

interpreter:
	g++ -o run/interpreter --std=c++11 run/interpreter-cln.cc -l cln

clean:
	rm -f *.o kompilator.out lex.yy.c* bison.tab.* run/*.mr run/interpreter stack.hh run/mojeTesty/*.mr

testAll: kompilator
	for input in run/*.imp; do \
		echo ;\
		echo ;\
		echo ;\
	    echo rm $${input%.imp}.mr -f;\
	    rm $${input%.imp}.mr -f;\
		echo ./kompilator.out $$input $${input%.imp}.mr; \
		./kompilator.out $$input $${input%.imp}.mr; \
	done

testAllMy: kompilator
	for input in run/mojeTesty/*.imp; do \
		echo ;\
		echo ;\
		echo ;\
	    echo rm $${input%.imp}.mr -f;\
	    rm $${input%.imp}.mr -f;\
		echo ./kompilator.out $$input $${input%.imp}.mr; \
		./kompilator.out $$input $${input%.imp}.mr; \
		echo ./run/interpreter $${input%.imp}.mr; \
		./run/interpreter $${input%.imp}.mr; \
	done

test:  kompilator interpreter
	rm run/$(FILE).mr -f
	./kompilator.out run/$(FILE).imp run/$(FILE).mr
	run/interpreter run/$(FILE).mr

program0: kompilator interpreter
	rm run/program0.mr -f
	./kompilator.out run/program0.imp run/program0.mr
	run/interpreter run/program0.mr

program1: kompilator interpreter
	rm run/program1.mr -f
	./kompilator.out run/program1.imp run/program1.mr
	run/interpreter run/program1.mr

program2: kompilator interpreter
	rm run/program2.mr -f
	./kompilator.out run/program2.imp run/program2.mr
	run/interpreter run/program2.mr

