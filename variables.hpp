#pragma once
#include "variable.hpp"
#include <string>
#include <map>

class Variables
{
public:
    bool createVariable(std::string id, long long length = 0);
    void deleteVariable(std::string id);
    bool checkId(std::string id);
    Variable& get(std::string id);
    unsigned long long getFreeAddress();
private:
    std::map <std::string, Variable> variables;
    unsigned long long freeAddress = 0;
};
