//
// Created by gumny on 05.01.18.
//

#pragma once

//macro for debug printing
//#define MY_DEBUG
#ifdef MY_DEBUG
#define DEBUG_PRINT(a) std::cerr<<a<<"\n";
#else
#define DEBUG_PRINT(a)
#endif

//Type used for identifier, it must be declared before bison.tab.hh
#include <string>
struct Identifier
{
    std::string name;
    unsigned long long address;
    bool isIndirect;
};
#include <cln/cln.h>

//Define signature of yylex function - it will return bison variant type, instead of int type. It is used both in bison and flex file, so the function can be forward declared.
#define YY_DECL yy::parser::symbol_type yylex ()

// fix for bug in bison, it should not need YY_NULLPTR in header
// https://stackoverflow.com/questions/34570823/make-yylex-return-symbol-type-instead-of-int#comment56974745_34570823
# define YY_NULLPTR nullptr

#include "bison.tab.hh"
