#pragma once

class Variable{
public:
    unsigned long long address;
    long long int size;
    bool isInitialised;
    void set(unsigned long long address, long long size);
    unsigned long long getAddress();
};
