//use C++ skeleton
%skeleton "lalr1.cc"
//use variant type for tokens - enables to conveniently return different types of objects from flex.
%define api.value.type variant
//define constructors for tokens
%define api.token.constructor
//variant types must be constructed and destructed properly
%define parse.assert

%{
    #include <iostream>
    #include <cstdarg>
    #include <vector>
    #include <string>
    #include <climits>
    #include "variables.hpp"
    #include "bisonWrapper.hpp"

    extern int yylineno;
    extern FILE* yyin;
    extern FILE *yyout;
    extern char* yytext;
    YY_DECL; // declare yylex()

    std::vector<std::string> code;
    std::vector<int> linesStack;
    void addToCode(const char* str, ...);
    void addToCode(int poz, const char* str, ...);
    void writeValueToRegister(const cln::cl_I&);
    int divide(int a_adr, int b_adr);
    int mod(int a_adr, int b_adr);
    bool isValid = true;
    Variables variables;
%}

//flex tokens
%token      VAR
%token      BEG
%token      END
%token      IF
%token      ELSE
%token      THEN
%token      ENDIF
%token      WHILE
%token      DO
%token      ENDWHILE
%token      FOR
%token      FROM
%token      TO
%token      ENDFOR
%token      DOWNTO
%token      READ
%token      WRITE
%token      SET
%token      NEQ
%token      GEQ
%token      LEQ
%token      ENDoF      0    "end of file"
%token      LBRACKET        "["
%token      RBRACKET        "]"
%token      PLUS            "+"
%token      MINUS           "-"
%token      MULTI           "*"
%token      DIV             "/"
%token      MOD             "%"
%token      EQ              "="
%token      LESS            "<"
%token      GREATER         ">"
%token      COLON           ";"
%token      <std::string>           PIDENTIFIER
%token      <cln::cl_I>             NUM
%type       <Identifier>            identifier
%type       <unsigned long long>    value
%type       <unsigned long long>    expression

%%
program
    : VAR vdeclarations BEG commands END
    {
        addToCode("HALT");
    }
    ;

vdeclarations
    : vdeclarations PIDENTIFIER
    {
        if( not variables.createVariable($2) )
            yy::parser::error(std::string("redeclaration of variable ") + $2);
    }
    | vdeclarations PIDENTIFIER "[" NUM "]"
    {
        if( not variables.createVariable($2, cln::cl_I_to_int($4)) )
            yy::parser::error(std::string("redeclaration of variable") + $2);
    }
    |
    ;

commands
    : commands command
    | command
    ;

ifBlock
    : IF condition THEN
    {
        linesStack.push_back(code.size());
        addToCode("Place for JZERO");
    } commands
    ;

command
    : identifier SET expression ";"
    {
        Identifier id = $1;
        Variable& var = variables.get(id.name);
        var.isInitialised = 1;
        if(var.size < 0)
            yy::parser::error(std::string("Modyfing iterator: ") + id.name);
        addToCode("LOAD %d", $3);
        if( id.isIndirect ){
            addToCode("STOREI %d", id.address);
        } else {
            addToCode("STORE %d", id.address);
        }
    }
    | READ identifier ";"
    {
        Identifier id = $2;
        Variable& var = variables.get(id.name);
        var.isInitialised = 1;
        if(var.size < 0)
            yy::parser::error(std::string("Modyfing iterator: ") + id.name);
        addToCode("GET");

        if( id.isIndirect )
        {
            addToCode("STOREI %d", id.address);
        } else {
            addToCode("STORE %d", id.address);
        }
    }
    | WRITE value ";"
    {
        addToCode("LOAD %d", $2);
        addToCode("PUT");
    }
    | ifBlock ENDIF
    {
        addToCode(linesStack.back(), "JZERO %d", code.size());
        linesStack.pop_back();
    }
    | ifBlock ELSE
    {
        addToCode(linesStack.back(), "JZERO %d ", code.size() + 1);
        linesStack.pop_back();
        linesStack.push_back(code.size());
        addToCode("Place for JUMP");
    } commands
    {
        addToCode(linesStack.back(), "JUMP %d", code.size());
        linesStack.pop_back();
    } ENDIF
    | WHILE
    {
        linesStack.push_back(code.size());
    } condition DO
    {
        linesStack.push_back(code.size());
        addToCode("Place for JZERO");
    } commands ENDWHILE
    {
        addToCode(linesStack.back(), "JZERO %d", code.size() + 1);
        linesStack.pop_back();
        addToCode("JUMP %d", linesStack.back());
        linesStack.pop_back();
    }
    | FOR PIDENTIFIER FROM value TO value DO
    {
        if( not variables.createVariable($2, -1) )
            yy::parser::error(std::string("redeclaration of variable ") + $2);

        auto i = variables.get($2).getAddress();
        auto a = $4;
        auto b = $6;
        auto bcopy = variables.getFreeAddress();

        addToCode("LOAD %d", a);
        addToCode("STORE %d", i);
        addToCode("LOAD %d", b);
        addToCode("INC");
        addToCode("STORE %d", bcopy);

        linesStack.push_back(code.size()); //do tego miejsca będziemy skakać

        addToCode("LOAD %d", bcopy);
        addToCode("SUB %d", i);
        linesStack.push_back(code.size());
        addToCode("Place for JUMP");
    } commands ENDFOR
    {
        auto i = variables.get($2).getAddress();

        addToCode("LOAD %d", i);
        addToCode("INC");
        addToCode("STORE %d", i);

        addToCode(linesStack.back(), "JZERO %d", code.size() + 1);
        linesStack.pop_back();
        addToCode("JUMP %d", linesStack.back());
        linesStack.pop_back();

        variables.deleteVariable($2);
    }
    | FOR PIDENTIFIER FROM value DOWNTO value DO
    {
        if( not variables.createVariable($2, -1) )
            yy::parser::error(std::string("redeclaration of variable ") + $2);

        auto i = variables.get($2).getAddress();
        auto a = $4;
        auto b = $6;
        auto bcopy = variables.getFreeAddress();

        addToCode("LOAD %d", b);
        addToCode("STORE %d", bcopy);

        addToCode("LOAD %d", a);
        addToCode("INC"); //zwiększamy wartość iteratora, bo chcemy ją zmniejszać już po porównaniu
        addToCode("STORE %d", i);

        //do tego miejsca będziemy skakać
        linesStack.push_back(code.size());

        addToCode("SUB %d", bcopy); // i + 1 - bcopy
        linesStack.push_back(code.size());
        addToCode("Place for JUMP");

        addToCode("LOAD %d", i);
        addToCode("DEC");                    //zmniejszenie iteratora
        addToCode("STORE %d", i);
    }commands ENDFOR
    {
        auto i = variables.get($2).getAddress();

        addToCode("LOAD %d", i);

        addToCode(linesStack.back(), "JZERO %d", code.size() + 1);
        linesStack.pop_back();
        addToCode("JUMP %d", linesStack.back());
        linesStack.pop_back();

        variables.deleteVariable($2);
    }
    ;

expression
    : value
    {
        $$ = $1;
    }
    | value "+" value
    {
        addToCode("LOAD %d", $1);
        addToCode("ADD %d", $3);
        $$ = variables.getFreeAddress();
        addToCode("STORE %d", $$);
    }
    | value "-" value
    {
        addToCode("LOAD %d", $1);
        addToCode("SUB %d", $3);
        $$ = variables.getFreeAddress();
        addToCode("STORE %d", $$);
    }
    | value "*" value
    {
        auto a = $1;
        auto b = $3;
        auto acopy = variables.getFreeAddress();
        auto bcopy = variables.getFreeAddress();
        auto mul = variables.getFreeAddress();

        addToCode("ZERO");
        addToCode("STORE %d", mul);
        addToCode("LOAD %d", a);
        addToCode("STORE %d", acopy);
        addToCode("LOAD %d", b);
        addToCode("STORE %d", bcopy);

        int begin = code.size();
        addToCode("JZERO %d", begin + 13);
        addToCode("JODD %d", begin + 3);
        addToCode("JUMP %d", begin + 6);

        //begin + 3
        addToCode("LOAD %d", mul);
        addToCode("ADD %d", acopy);
        addToCode("STORE %d", mul);

        //begin + 6
        addToCode("LOAD %d", acopy);
        addToCode("SHL");
        addToCode("STORE %d", acopy);
        addToCode("LOAD %d", bcopy);
        addToCode("SHR");
        addToCode("STORE %d", bcopy);
        addToCode("JUMP %d", begin);

        //begin + 13
        $$ = mul;
    }
    | value "/" value
    {
        $$ = divide($1, $3);
    }
    | value "%" value
    {
        $$ = mod($1, $3);
    }
    ;

condition
    : value "=" value
    {
        //NEQ
        auto adr = variables.getFreeAddress();
        addToCode("LOAD %d", $1);   //a = x
        addToCode("SUB %d", $3);    //a = x - y
        addToCode("STORE %d", adr); //adr = x - y
        addToCode("LOAD %d", $3);   //a = y
        addToCode("SUB %d" , $1);   //a = y - x
        addToCode("ADD %d", adr);   //a = (y - x) + (x - y) = 0 tylko, jeżeli są równe

        //negacja odpowiedzi
        int address = code.size();
        addToCode("JZERO %d ", address + 3);
        addToCode("ZERO");
        addToCode("JUMP %d ", address + 4);
        addToCode("INC");
    }
    | value NEQ value
    {
        auto adr = variables.getFreeAddress();
        addToCode("LOAD %d", $1);   //a = x
        addToCode("SUB %d", $3);    //a = x - y
        addToCode("STORE %d", adr); //adr = x - y
        addToCode("LOAD %d", $3);   //a = y
        addToCode("SUB %d" , $1);   //a = y - x
        addToCode("ADD %d", adr);   //a = (y - x) + (x - y) = 0 tylko, jeżeli są równe
    }
    | value "<" value
    {
        addToCode("LOAD %d", $3);
        addToCode("SUB %d", $1);
    }
    | value ">" value
    {
        addToCode("LOAD %d", $1);
        addToCode("SUB %d", $3);
    }
    | value LEQ value
    {
        addToCode("LOAD %d", $3);
        addToCode("INC");
        addToCode("SUB %d", $1);
    }
    | value GEQ value
    {
        addToCode("LOAD %d", $1);
        addToCode("INC");
        addToCode("SUB %d", $3);
    }
    ;

value
    : NUM
    {
        writeValueToRegister($1);
        $$ = variables.getFreeAddress();
        addToCode("STORE %d", $$);
    }
    | identifier
    {
        if(! variables.get($1.name).isInitialised)
            yy::parser::error(std::string("Using value of an uninitialised variable ") + $1.name);
        if($1.isIndirect)
        {
            addToCode("LOADI %d", $1.address);
            $$ = variables.getFreeAddress();
            addToCode("STORE %d", $$);
        }else{
            $$ = $1.address;
        }
    }
    ;

identifier
    : PIDENTIFIER
    {
        if( variables.checkId($1) )
        {
            Variable& var = variables.get($1);
            if(var.size > 0)
                yy::parser::error(std::string("array ") + $1 + " is used as a scalar");
            else
            {
                $$ = {$1, variables.get($1).getAddress(), false};
            }
        }
        else
            yy::parser::error(std::string("undeclared variable ") + $1);
    }
    | PIDENTIFIER "[" PIDENTIFIER "]"
    {
        if( variables.checkId($3) )
        {
            Variable& index = variables.get($3);
            if(index.size > 0)
                yy::parser::error(std::string("array ") + $3 + " is used as a scallar");
            else if( variables.checkId($1) )
            {
                Variable& var = variables.get($1);
                if(var.size <= 0) //check
                    yy::parser::error(std::string("scalar ") + $1 + " is used as an array");
                else
                {
                    writeValueToRegister(var.getAddress());
                    addToCode("ADD %d", index.getAddress());
                    auto adr = variables.getFreeAddress();
                    addToCode("STORE %d", adr);
                    $$ = {$1, adr, true};
                }
            } else
                yy::parser::error(std::string("undeclared variable ") + $1);
        }else
            yy::parser::error(std::string("undeclared variable ") + $3);
    }
    | PIDENTIFIER "[" NUM "]"
    {
        if( variables.checkId($1) )
        {
            Variable& var = variables.get($1);
            auto adr = cln::cl_I_to_int($3);
            if(var.size <= 0)
                yy::parser::error(std::string("scalar ") + $1 + " is used as an array");
            else if(var.size <= $3)
                yy::parser::error("Index out of bounds: "+$1+"["+std::to_string(adr)+"], sizeof(" + $1 + ") = " + std::to_string(var.size) );
            $$ = {$1, var.getAddress() + adr, false};
        }else
            yy::parser::error(std::string("undeclared variable ") + $1);
    }
    ;

%%
int main(int argc, char *argv[])
{
    if(argc == 1 || argc > 3)
    {
        printf("Usage: %s inputFile [outputFile = stdout]", argv[0]);
        return 0;
    }

    yyin = fopen( argv[1], "r" );
    yy::parser parser;
    parser.parse();
    if(isValid)
    {
        if(argc == 3)
            yyout = fopen(argv[2],"w");
        for(std::vector<std::string>::iterator it = code.begin(); it<code.end(); it++)
            fprintf(yyout, "%s\n", it->c_str());
    }
    return 0;
}

void addToCode(const char* str, ...){
    char buffer[256];
    va_list args;
    va_start (args, str);
    vsprintf (buffer, str, args);
    DEBUG_PRINT(code.size()<<": CODE: "<<buffer);
    code.push_back(buffer);
    va_end (args);
}

void addToCode(int poz, const char* str, ...){
    char buffer[256];
    va_list args;
    va_start (args, str);
    vsprintf (buffer, str, args);
    DEBUG_PRINT(poz<<": CODE["<<poz<<"]: "<<buffer);
    code[poz] = buffer;
    va_end (args);
}

void writeValueToRegister(const cln::cl_I& value)
{
    addToCode("ZERO");
    bool shifting=0;

    auto i = cln::integer_length(value) + 1;
    do {
        i--;
        if(shifting)
            addToCode("SHL");
        if( cln::logbitp(i, value) )
        {
            addToCode("INC");
            shifting = 1;
        }
    } while(i>0);
}

std::pair<int, int> divide_with_mod(int a, int b)
{
    //https://en.wikipedia.org/wiki/Division_algorithm#Integer_division_(unsigned)_with_remainder
    auto acopy = variables.getFreeAddress();
    auto arev = variables.getFreeAddress();
    auto mod = variables.getFreeAddress();
    auto div = variables.getFreeAddress();

    addToCode("ZERO");
    addToCode("STORE %d", mod);
    addToCode("STORE %d", div);
    addToCode("LOAD %d", a);
    addToCode("STORE %d", acopy);

    int begin = code.size();
    addToCode("LOAD %d", b);
    addToCode("JZERO %d", begin + 52); //address
    //reverse bits of acopy, to get its most significant bit first, but add 1 at the beginning, so it doesn't get truncated:

    addToCode("ZERO");
    addToCode("INC");
    addToCode("STORE %d", arev);
    addToCode("LOAD %d", acopy);
    int reversing = code.size();
    addToCode("JODD %d", reversing + 6);
    addToCode("JZERO %d", reversing + 14);

    addToCode("LOAD %d", arev);
    addToCode("SHL");
    addToCode("STORE %d", arev);
    addToCode("JUMP %d", reversing + 10);

    //reversing + 6
    addToCode("LOAD %d", arev);
    addToCode("SHL");
    addToCode("INC");
    addToCode("STORE %d", arev);

    //reversing + 10
    addToCode("LOAD %d", acopy);
    addToCode("SHR");
    addToCode("STORE %d", acopy);
    addToCode("JUMP %d", reversing);

    //reversing + 14
    addToCode("LOAD %d", arev);
    int dividing = code.size();
    addToCode("JODD %d", dividing + 5);

    addToCode("LOAD %d", mod);
    addToCode("SHL");
    addToCode("STORE %d", mod);
    addToCode("JUMP %d", dividing + 11);

    //dividing + 5
    addToCode("DEC");
    addToCode("JZERO %d", dividing + 28); //skok na koniec, zostaliśmy z samą dołożoną jedynką

    addToCode("LOAD %d", mod);
    addToCode("SHL");
    addToCode("INC");
    addToCode("STORE %d", mod);

    //dividing + 11
    addToCode("INC");
    addToCode("SUB %d", b);
    addToCode("JZERO %d", dividing + 21); // za if
    addToCode("DEC");
    addToCode("STORE %d", mod); //mod -= b

    addToCode("LOAD %d", div);
    addToCode("INC");
    addToCode("SHL");
    addToCode("STORE %d", div);
    addToCode("JUMP %d", dividing + 24);

    //dividing + 21
    addToCode("LOAD %d", div);
    addToCode("SHL");
    addToCode("STORE %d", div);

    //dividing + 24
    addToCode("LOAD %d", arev);
    addToCode("SHR");
    addToCode("STORE %d", arev);
    addToCode("JUMP %d", dividing);

    //dividing + 28
    addToCode("LOAD %d", div);
    addToCode("SHR");
    addToCode("STORE %d", div);

    //begin + 52
    return {div, mod};
}
int divide(int a_adr, int b_adr)
{
    return divide_with_mod(a_adr, b_adr).first;
}
int mod(int a_adr, int b_adr)
{
    return divide_with_mod(a_adr, b_adr).second;
}
void yy::parser::error (const std::string& error)
{
    std::cerr<<"Error: Line "<<yylineno<<" at \""<<yytext<<"\":"<<std::endl<<"    "<<error<<".\n";
    isValid = false;
}
